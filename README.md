ansible-role-alacritty
=========

This role installs alacritty on Alpine Linux, Fedora and Pop!OS. Ubuntu is unsupported as long as it doesn't have a package.

License
-------

BSD
